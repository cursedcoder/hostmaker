const AWS = require('aws-sdk');
const gulp = require('gulp');
const awspublish = require('gulp-awspublish');
const cloudfront = require('gulp-cloudfront-invalidate-aws-publish');

AWS.config.region = 'eu-west-1';

const cf = new AWS.CloudFront();

const findDistribution = name => cf.listDistributions()
  .promise()
  .then((response) => {
    return response.Items.find((item) => {
      return item.Origins.Items.find(innerItem => innerItem.Id.includes(name));
    });
  });

gulp.task('default', () => {
  const publisher = awspublish.create({
    region: 'eu-west-1',
    params: {
      Bucket: 'hostmaker',
    },
  });

  const cfSettings = id => ({
    distribution: id,
    wait: false,
    originPath: '/',
    indexRootPath: true,
  });

  return findDistribution('hostmaker')
    .then((distribution) => {
      return gulp.src('./build/**/*')
        .pipe(publisher.publish({}, { force: true }))
        .pipe(publisher.sync())
        .pipe(cloudfront(cfSettings(distribution.Id)))
        .pipe(awspublish.reporter());
    });
});
