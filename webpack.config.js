const slsw = require('serverless-webpack');

module.exports = {
  target: 'node',
  externals: 'aws-sdk',
  entry: slsw.lib.entries,
  mode: slsw.lib.webpack.isLocal ? 'development' : 'production',
};
