const NotFound = require('./notFound');

const headers = {
  'Access-Control-Allow-Origin': '*',
};

/**
 * Simply AWS response for API Gateway.
 *
 * @param handler
 * @return {function(event, context, callback)}
 */
module.exports = function response(handler) {
  return async (event, context) => {
    try {
      const data = await handler(event, context);

      return {
        statusCode: 200,
        body: JSON.stringify(data),
        headers,
      };
    } catch (error) {
      if (error instanceof NotFound) {
        return {
          statusCode: 404,
          body: JSON.stringify({ message: error.message }),
          headers,
        };
      }

      return {
        statusCode: 500,
        headers,
      };
    }
  };
};
