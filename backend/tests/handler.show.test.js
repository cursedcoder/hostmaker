process.env.IS_OFFLINE = true;

const { expect } = require('chai');
const model = require('../model/property');
const handler = require('../handler/show');

describe('Show handler', () => {
  beforeEach(async () => {
    const properties = await model.list();
    await Promise.all(properties.map(p => model.remove(p.id)));
  });

  it('should return response with properties show', async () => {
    await model.create({
      id: 'some-testing-id',
    });

    await model.create({
      id: 'some-testing-id-2',
    });

    const response = await handler.show({ pathParameters: { id: 'some-testing-id' } });
    expect(response.statusCode).to.be.eq(200);
    expect(response.body).to.be.eq('{"id":"some-testing-id"}');
  });

  it('should return 404 if property not found', async () => {
    const response = await handler.show({ pathParameters: { id: 'anybody lives in this property?' } });

    expect(response.statusCode).to.be.eq(404);
  });
});
