process.env.IS_OFFLINE = true;

const { expect } = require('chai');
const model = require('../model/property');

describe('Property Model', () => {
  beforeEach(async () => {
    const properties = await model.list();
    await Promise.all(properties.map(p => model.remove(p.id)));
  });

  it('should be able to create property', async () => {
    await model.create({
      id: 'some-testing-id',
      owner: 'Tester',
      address: {
        line1: 'SOme Road',
      },
    });

    const properties = await model.list();
    expect(properties.length).to.be.eq(1);
    expect(properties[0].owner).to.be.eq('Tester');
    expect(properties[0].address).to.be.an('object');
    expect(properties[0].address.line1).to.be.eq('SOme Road');
  });

  it('should be able to remove property', async () => {
    await model.create({
      id: 'delete-me',
    });

    const properties1 = await model.list();
    expect(properties1.length).to.be.eq(1);

    await model.remove('delete-me');

    const properties2 = await model.list();
    expect(properties2.length).to.be.eq(0);
  });

  it('should be able to show property', async () => {
    await model.create({
      id: 'some-testing-id',
      owner: 'Tester',
      address: {
        line1: 'SOme Road',
      },
    });

    const property = await model.show('some-testing-id');

    expect(property).to.be.an('object');
    expect(property.owner).to.be.eq('Tester');
  });
});
