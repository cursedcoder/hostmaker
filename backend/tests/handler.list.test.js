process.env.IS_OFFLINE = true;

const { expect } = require('chai');
const model = require('../model/property');
const handler = require('../handler/list');

describe('List handler', () => {
  beforeEach(async () => {
    const properties = await model.list();
    await Promise.all(properties.map(p => model.remove(p.id)));
  });

  it('should return response with properties list', async () => {
    await model.create({
      id: 'some-testing-id',
    });

    await model.create({
      id: 'some-testing-id-2',
    });

    const response = await handler.list();

    expect(response.statusCode).to.be.eq(200);
    expect(response.body).to.be.eq('[{"id":"some-testing-id"},{"id":"some-testing-id-2"}]');
  });
});
