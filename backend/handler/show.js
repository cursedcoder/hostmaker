const response = require('../aws/response');
const NotFound = require('../aws/notFound');
const property = require('../model/property');

module.exports.show = response(async (data) => {
  const result = await property.show(data.pathParameters.id);

  if (!result) {
    throw new NotFound('Property not found.');
  }

  return result;
});
