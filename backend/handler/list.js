const response = require('../aws/response');
const property = require('../model/property');

module.exports.list = response(property.list);
