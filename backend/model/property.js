const dynamodb = require('../aws/dynamodb');
const table = require('../config/table');

function list() {
  return dynamodb.scan({ TableName: table.PROPERTY_TABLE_NAME })
    .promise()
    .then(response => response.Items);
}

async function show(id) {
  try {
    return await dynamodb.get({
      TableName: table.PROPERTY_TABLE_NAME,
      Key: {
        id,
      },
    }).promise()
      .then(response => response.Item);
  } catch (e) {
    throw new Error(`Unexpected error during show: ${e.message}.`);
  }
}

async function remove(id) {
  try {
    await dynamodb.delete({
      TableName: table.PROPERTY_TABLE_NAME,
      Key: {
        id,
      },
    }).promise();
  } catch (e) {
    throw new Error(`Unexpected error during deletion: ${e.message}.`);
  }
}

async function create(property) {
  try {
    await dynamodb.put({
      TableName: table.PROPERTY_TABLE_NAME,
      Item: {
        ...property,
      },
    }).promise();
  } catch (e) {
    throw new Error(`Unexpected error during creation: ${e.message}.`);
  }
}

module.exports = {
  create,
  remove,
  list,
  show,
};
