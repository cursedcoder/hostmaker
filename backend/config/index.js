const aws = require('./aws');
const table = require('./table');

module.exports = {
  aws, table,
};
