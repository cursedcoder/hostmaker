const faker = require('faker');
const fs = require('fs');
const os = require('os');

const POSTCODES_FILE = `${__dirname}/postcodes.txt`;
const STREETS_FILE = `${__dirname}/streets.txt`;

function getRandomLine(fileName) {
  const lines = fs.readFileSync(fileName).toString().split(os.EOL);

  if (lines.length === 0) {
    throw new Error(`${fileName} is empty`);
  }

  return faker.random.arrayElement(lines);
}

function createBasicData() {
  return {
    owner: faker.name.firstName(),
    address: {
      line1: faker.address.secondaryAddress(),
      line4: getRandomLine(STREETS_FILE),
      postCode: getRandomLine(POSTCODES_FILE),
      city: 'London',
      country: 'U.K.',
    },
    airbnbId: faker.random.number({ min: 3515500, max: 3715500 }),
    numberOfBedrooms: faker.random.number({ min: 1, max: 5 }),
    numberOfBathrooms: faker.random.number({ min: 1, max: 5 }),
    incomeGenerated: (faker.random.number({ min: 100, max: 20000 }) + Math.random()).toFixed(2),
  };
}

function createVariativeData() {
  const basic = createBasicData();

  return Object.assign(
    basic,
    { id: faker.random.uuid() },
    faker.random.boolean()
      ? { address: { ...basic.address, line2: faker.address.streetAddress() } } : null,
    faker.random.boolean()
      ? { address: { ...basic.address, line3: faker.address.streetAddress() } } : null,
  );
}

module.exports = {
  generateUKPostCode: getRandomLine,
  createBasicData,
  createVariativeData,
};
