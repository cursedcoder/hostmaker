#!/usr/bin/env node

process.env.IS_OFFLINE = true;

const fs = require('fs');
const mini = require('minimist');
const propertyFaker = require('../faker/propertyFaker');
const property = require('../model/property');

const argv = mini(process.argv.slice(2));
const generateRecords = argv.c || 25;

const basicEntities = JSON.parse(fs.readFileSync(`${__dirname}/../../data/example.json`).toString());
const generatedEntities = Array.from({ length: generateRecords })
  .map(() => propertyFaker.createVariativeData());

const entities = basicEntities.concat(generatedEntities);

Promise
  .all(entities.map(property.create))
  .then(() => console.log(`Successfully populated database with ${entities.length} record(s)`));
