Superior property details manager
=================================

Install:

    npm install
    
Install dynamodb local (once)

    npm run dynamodb:install

Run serverless offline

    npm run sls:start

Populate database

    npm run populate

To run checks and etc:

    npm run lint
    npm run sls:test
    npm run react:test
