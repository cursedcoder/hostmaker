import Geocode from 'react-geocode';
import { SERVICE_AREA_LAT, SERVICE_AREA_LNG, SERVICE_AREA_RADIUS_KM } from './serviceArea';

// set Google Maps Geocoding API for purposes of quota management. Its optional but recommended.
Geocode.setApiKey('AIzaSyCtkUmWcvYsiRdcz8OD0Owdf2OnyKhwX9U');

// Enable or disable logs. Its optional.
Geocode.enableDebug();

function deg2rad(deg) {
  return deg * (Math.PI / 180);
}

function getDistanceFromLatLonInKilometers(lat1, lon1, lat2, lon2) {
  const R = 6371;

  const dLat = deg2rad(lat2 - lat1);
  const dLon = deg2rad(lon2 - lon1);

  const a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
    + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
    * Math.sin(dLon / 2) * Math.sin(dLon / 2);

  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  return R * c;
}

export async function addressToLatLng(address) {
  try {
    const response = await Geocode.fromAddress(address);

    return response.results[0].geometry.location;
  } catch (e) {
    console.error(e);

    throw e;
  }
}

export async function addressWithinArea(address, lat, lng, radius) {
  const addressLocation = await addressToLatLng(address);

  const distanceKm = getDistanceFromLatLonInKilometers(
    lat,
    lng,
    addressLocation.lat,
    addressLocation.lng,
  );

  console.info(`Distance: ${distanceKm.toFixed(2)}km`);

  return distanceKm < radius;
}

export async function addressWithinServiceArea(address) {
  return addressWithinArea(
    address,
    SERVICE_AREA_LNG,
    SERVICE_AREA_LAT,
    SERVICE_AREA_RADIUS_KM,
  );
}
