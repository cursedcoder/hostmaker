import axios from 'axios';
import config from '../config';

export const instance = axios.create({
  baseURL: config[process.env.REACT_APP_STAGE],
});

const request = (type, url) => instance[type](url).then(response => response.data);

export function listProperties() {
  return request('get', '/property');
}

export function showProperty(id) {
  return request('get', `/property/${id}`);
}
