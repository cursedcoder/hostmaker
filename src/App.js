import React, { Component } from 'react';
import PropTypes from 'prop-types';
import debounce from 'debounce';
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import { CircularProgress, CssBaseline, Grid } from '@material-ui/core';

import theme from './theme';
import Header from './Component/Header';
import Property from './Component/Property';

import * as endpoints from './api/endpoints';

const styles = {
  root: {
    justifyContent: 'center',
    marginTop: 10,
  },
};

class App extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
  };

  updateFilter = debounce((filter) => {
    this.setState({ filter });
  }, 400);

  constructor() {
    super();

    this.state = {
      loading: true,
      properties: [],
      filter: '',
    };
  }

  async componentDidMount() {
    const properties = await endpoints.listProperties();
    this.setState({ properties, loading: false });
  }

  filteredProperties() {
    const { properties, filter } = this.state;

    if (!filter) {
      return properties;
    }

    return properties.filter(prop => JSON
      .stringify(prop)
      .toLowerCase()
      .includes(filter.toLowerCase()))
      .sort((a, b) => a.id > b.id);
  }

  renderProperties() {
    const { loading } = this.state;

    if (loading) {
      return <CircularProgress />;
    }

    return this.filteredProperties().map(property => (
      <Grid key={property.id} item xl={3} lg={4} sm={6} xs={12}>
        <Property {...property} />
      </Grid>
    ));
  }

  render() {
    const { classes } = this.props;

    return (
      <MuiThemeProvider theme={theme}>
        <Header
          filterHandler={this.updateFilter}
        />
        <Grid container className={classes.root}>
          <CssBaseline />
          {this.renderProperties()}
        </Grid>
      </MuiThemeProvider>
    );
  }
}

export default withStyles(styles, { name: 'App' })(App);
