import 'jsdom-global/register';
import React from 'react';
import { configure } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16';
import { createShallow } from '@material-ui/core/test-utils';

import App from './App';
import Header from './Component/Header';
import Property from './Component/Property';

configure({ adapter: new Adapter() });

const firstProperty = {
  id: 'some uuid',
  owner: 'Carlos',
  address: {
    country: 'U.K.',
    city: 'London',
    line: 'Somewhere',
  },
  incomeGenerated: '100500',
};

const secondProperty = {
  id: 'some uuid 2',
  owner: 'Mary',
  address: {
    country: 'U.K.',
    city: 'London',
    line: 'Somewhere else',
  },
  incomeGenerated: '200',
};

describe('App component testing', () => {
  it('renders application with single property', () => {
    const wrapper = createShallow()(<App />, {
      disableLifecycleMethods: true,
    });

    const dive = wrapper.dive();

    expect(wrapper.prop('classes')).to.be.an('object');
    expect(dive.find(Header)).to.have.lengthOf(1);

    dive.setState({ loading: false, properties: [firstProperty] });

    expect(dive.update().find(Property)).to.have.lengthOf(1);

    const propertyHtml = dive.find(Property).html();

    expect(propertyHtml).to.have.string('£100500');
    expect(propertyHtml).to.have.string('Carlos');
    expect(propertyHtml).to.have.string('London');
    expect(propertyHtml).to.have.string('U.K.');
  });

  it('renders filtered amount of properties', () => {
    const wrapper = createShallow()(<App />, {
      disableLifecycleMethods: true,
    });

    const dive = wrapper.dive();

    dive.setState({ loading: false, properties: [firstProperty, secondProperty] });

    expect(dive.update().find(Property)).to.have.lengthOf(2);

    dive.setState({ filter: 'Mary' });

    expect(dive.update().find(Property)).to.have.lengthOf(1);

    dive.setState({ filter: 'Nobody' });

    expect(dive.update().find(Property)).to.have.lengthOf(0);
  });
});
