import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Home from '@material-ui/icons/Home';

import {
  Grid,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Card,
  CardContent,
  CardActions,
  Typography,
  Button,
  LinearProgress,
} from '@material-ui/core';
import { addressWithinServiceArea } from '../api/maps';

const styles = {
  card: {
    minWidth: 275,
    margin: 10,
  },
  cardActions: {
    justifyContent: 'center',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  addressItem: {
    padding: '0 !important',
  },
  addressItemIcon: {
    marginRight: '0 !important',
  },
};

class Property extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    owner: PropTypes.string.isRequired,
    incomeGenerated: PropTypes.string.isRequired,
    address: PropTypes.object.isRequired,
  };

  constructor() {
    super();

    this.state = {
      withinServiceArea: null,
      loading: false,
    };
  }

  get locationAddress() {
    const { address } = this.props;

    return [
      address.line4,
      address.city,
      address.country,
    ].filter(v => v).join(', ');
  }

  get formattedAddress() {
    const { address } = this.props;

    return [
      address.line1,
      address.line2,
      address.line3,
      address.line4,
      address.city,
      address.country,
    ].filter(v => v).join(', ');
  }

  checkServiceArea() {
    this.setState({
      loading: true,
    }, async () => {
      const withinServiceArea = await addressWithinServiceArea(this.locationAddress);

      this.setState({
        loading: false,
        withinServiceArea,
      });
    });
  }

  renderStatus() {
    const { loading, withinServiceArea } = this.state;

    if (loading) {
      return <LinearProgress />;
    }

    if (withinServiceArea === true) {
      return <div style={{ height: 5, backgroundColor: '#13c71b' }} />;
    }

    if (withinServiceArea === false) {
      return <div style={{ height: 5, backgroundColor: '#e10011' }} />;
    }

    return <div style={{ height: 5 }} />;
  }

  render() {
    const {
      classes,
      owner,
      incomeGenerated,
    } = this.props;

    const { loading, withinServiceArea } = this.state;

    return (
      <Card className={classes.card}>
        {this.renderStatus()}
        <CardContent>
          <Grid container>
            <Grid item xl={9} lg={8} sm={6} xs={6}>
              <Typography className={classes.pos} color="textSecondary">
                Owner
              </Typography>
            </Grid>

            <Grid item xl={3} lg={4} sm={6} xs={6}>
              <Typography className={classes.pos} color="textSecondary">
                Income generated
              </Typography>
            </Grid>

            <Grid item xl={9} lg={8} sm={6} xs={6}>
              <Typography variant="headline" component="h2">
                {owner}
              </Typography>
            </Grid>

            <Grid item xl={3} lg={4} sm={6} xs={6}>
              <Typography variant="display1" component="span">
                £
                {parseInt(incomeGenerated, 10)}
              </Typography>
            </Grid>

            <List>
              <ListItem className={classes.addressItem}>
                <ListItemIcon className={classes.addressItemIcon}>
                  <Home />
                </ListItemIcon>

                <ListItemText>
                  {this.formattedAddress}
                </ListItemText>
              </ListItem>
            </List>
          </Grid>
        </CardContent>
        <CardActions className={classes.cardActions}>
          <Button
            size="small"
            disabled={loading || (withinServiceArea !== null)}
            onClick={() => this.checkServiceArea()}
          >
            Check service area
          </Button>
        </CardActions>
      </Card>
    );
  }
}

export default withStyles(styles, { name: 'Property' })(Property);
